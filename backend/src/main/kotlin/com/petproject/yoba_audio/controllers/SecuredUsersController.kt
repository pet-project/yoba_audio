package com.petproject.yoba_audio.controllers

import com.petproject.yoba_audio.entities.PrivateUserData
import com.petproject.yoba_audio.services.UserAuthenticationService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

const val SECURED_USER_CONTROLLER_PATH = "/users"
const val CURRENT = "/current"
const val LOGOUT = "/logout"

@RestController
@RequestMapping(SECURED_USER_CONTROLLER_PATH)
class SecuredUsersController(private val auth: UserAuthenticationService) {

    @GetMapping(CURRENT)
    fun getCurrent(@AuthenticationPrincipal privateUserData: PrivateUserData) = privateUserData

    @GetMapping(LOGOUT)
    fun logout(@AuthenticationPrincipal privateUserData: PrivateUserData): Boolean {
        auth.logout(privateUserData)
        return true
    }
}