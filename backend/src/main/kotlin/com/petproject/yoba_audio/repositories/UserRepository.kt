package com.petproject.yoba_audio.repositories


import com.petproject.yoba_audio.entities.PrivateUserData
import com.petproject.yoba_audio.entities.PublicUserData
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.Optional
import java.util.UUID

@Repository
interface UserRepository: CrudRepository<PrivateUserData, UUID> {
    fun save(privateUserData: PrivateUserData): PrivateUserData
    fun save(publicUserData: PublicUserData): PublicUserData
//    fun find(id: UUID): Optional<User>
    fun findByUsername(userName: String): Optional<PrivateUserData>
}