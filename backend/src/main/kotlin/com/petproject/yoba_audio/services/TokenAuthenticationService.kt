package com.petproject.yoba_audio.services

import com.petproject.yoba_audio.entities.PrivateUserData
import com.petproject.yoba_audio.repositories.UserRepository
import com.petproject.yoba_audio.token.TokenService
import com.petproject.yoba_audio.utils.computeHash
import org.springframework.stereotype.Service
import java.util.Optional

@Service
class TokenAuthenticationService(private val tokens: TokenService, private val userRepository: UserRepository): UserAuthenticationService {


    override fun login(username: String, password: String): Optional<String> = userRepository.findByUsername(username)
            .filter { computeHash(password) == it.password }
            .map { tokens.expiring(mapOf("username" to username)) }

    override fun findByToken(token: String) =
            userRepository.findByUsername(tokens.verify(token).getValue("username"))

    override fun logout(privateUserData: PrivateUserData) {
        // Nothing to do
    }
}