package com.petproject.yoba_audio.services

import com.petproject.yoba_audio.entities.PrivateUserData
import java.util.Optional

interface UserAuthenticationService {

    fun login(username: String, password: String): Optional<String>

    fun findByToken(token: String): Optional<PrivateUserData>

    fun logout(privateUserData: PrivateUserData)

}