package com.petproject.yoba_audio.logic

import com.petproject.yoba_audio.entities.PrivateUserData
import com.petproject.yoba_audio.entities.PublicUserData
import com.petproject.yoba_audio.repositories.UserRepository
import com.petproject.yoba_audio.utils.computeHash
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

interface UserLogic {
    fun register(username: String, password: String): PrivateUserData
    fun getCurrentUser(): PrivateUserData
}

@Component
class UserLogicImpl(private val userRepository: UserRepository): UserLogic {

    override fun register(username: String, password: String): PrivateUserData {
        userRepository.findByUsername(username).ifPresent{throw IllegalStateException("User with this username already exists")}
        val publicUserData = PublicUserData();
        val privateUserData = PrivateUserData(publicUserData, username, computeHash(password) )
        userRepository.save(publicUserData)
        return userRepository.save(privateUserData)
    }

    override fun getCurrentUser(): PrivateUserData =
            SecurityContextHolder.getContext().authentication.principal as PrivateUserData

}