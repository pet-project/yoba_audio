package com.petproject.yoba_audio.entities

import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class PublicUserData(
        @Id
        @Column(name = "id")
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        var id: UUID? = null,

        @Column
        var firstName: String? = "",

        @Column
        var SecondName: String? = "",

        @Column
        var LastName: String? = ""

) {

}