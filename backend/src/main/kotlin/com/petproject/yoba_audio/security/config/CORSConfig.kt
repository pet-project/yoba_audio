package com.petproject.yoba_audio.security.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter

@Configuration
class CORSConfig {
    @Bean
    fun filterRegistrationBean(corsConfig: CorsFilter?) =
        FilterRegistrationBean(corsConfig).apply { order = 0 }

    @Bean
    fun corsFilter(@Qualifier("corsConfigurationSource") corsConfig: CorsConfigurationSource) =
        CorsFilter(corsConfig)

    @Bean
    fun corsConfigurationSource() = UrlBasedCorsConfigurationSource()
        .apply { registerCorsConfiguration("/**", getCorsConfig()) }

    private fun getCorsConfig() = CorsConfiguration().apply {
        allowCredentials = true
        addAllowedOrigin("*")
        addAllowedHeader("*")
        addAllowedMethod("*")
    }

}