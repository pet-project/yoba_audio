package com.petproject.yoba_audio.security.config

import org.springframework.security.web.RedirectStrategy
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class NoRedirectStrategy : RedirectStrategy {

    override fun sendRedirect(request: HttpServletRequest, response: HttpServletResponse, url: String) {
        // No redirect is required with pure REST
    }
}