package com.petproject.yoba_audio.logic;

import com.petproject.yoba_audio.dto.AudioDto;
import com.petproject.yoba_audio.entities.AudioEntity;
import com.petproject.yoba_audio.services.AudioService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.xml.bind.DatatypeConverter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.nio.ByteBuffer.allocate;
import static java.util.Objects.requireNonNull;
import static org.springframework.util.StringUtils.cleanPath;

@Component
public class AudioBusinessLogic {

    private final AudioService audioService;
    private final UserLogic userLogic;
    private final RangeResolver rangeResolver;
    private final Path fileStorageLocation;

    @Autowired
    public AudioBusinessLogic(AudioService audioService, UserLogic userLogic, RangeResolver rangeResolver, @Value("${file.upload-path}") String path) {
        this.audioService = audioService;
        this.userLogic = userLogic;
        this.rangeResolver = rangeResolver;
        this.fileStorageLocation = Paths.get(path).toAbsolutePath().normalize();
    }

    public List<AudioDto> getCurrentUserAudios() throws FileNotFoundException {
        UUID currentUserId = userLogic.getCurrentUser().getId();
        Optional<List<AudioEntity>> userAudios = audioService.getUserAudios(currentUserId);
        if(userAudios.isEmpty())
            throw new FileNotFoundException(String.format("User( %s ) has no audios yet", currentUserId ));
        return userAudios.get().stream().map(AudioEntity::convertToDto).collect(Collectors.toList());
    }

    public void deleteAudio(UUID id) {
        audioService.deleteAudio(id);
    }

    public ResponseEntity<StreamingResponseBody> download(String id, String range) throws Exception {
        Resource resource = audioService.getAudioFile(UUID.fromString(id));
        ResourceRegion region = rangeResolver.resolve(range, resource).get(0);

        final int contentLength = (int) region.getCount();
        final long contentStartPosition = region.getPosition();

        StreamingResponseBody stream = out -> {
            try(final FileInputStream fileInputStream = new FileInputStream(resource.getFile())) {
                ByteBuffer buffer = allocate(contentLength);
                fileInputStream.getChannel().read(buffer, contentStartPosition);
                out.write(buffer.array(), 0 , contentLength);
            }
        };

        return makeResponse(contentLength, contentStartPosition, resource.getFile().length(), stream);
    }

    private ResponseEntity<StreamingResponseBody> makeResponse(final long requestedLength,
                                                               final long startPosition,
                                                               final long fileLength,
                                                               final StreamingResponseBody stream) {
        return ResponseEntity.ok()
                .header("Content-Length", String.valueOf(requestedLength))
                .header("Content-Range", getContentRange(requestedLength, startPosition, fileLength))
                .body(stream);
    }

    @NotNull
    private String getContentRange(long requestedLength, long startPosition, long fullLength) {
        return startPosition + "-" + (requestedLength + startPosition - 1) + "/" + fullLength;
    }

    public AudioDto upload(final MultipartFile file, final String author, final String title) throws IOException {
        final String checkSum = DatatypeConverter.printHexBinary(computeHash(file)).toUpperCase();
        if(audioService.getAudioByCheckSum(checkSum).isPresent())
            throw new FileAlreadyExistsException(String.format("Audio with checkSum( %s ) already exists!", checkSum));
        final String fileExtension = getExtensionFor(file);
        final Path location = fileStorageLocation.resolve(checkSum + fileExtension);
        final AudioEntity entity = makeEntity(author, title, location.toString(), checkSum);

        return audioService.uploadAudioFile(file, location, entity).convertToDto();
    }

    private String getExtensionFor(final MultipartFile file) {
        requireNonNull(file.getOriginalFilename());
        return cleanPath(file.getOriginalFilename())
                .substring(cleanPath(file.getOriginalFilename()).lastIndexOf('.'));
    }

    private byte[] computeHash(final MultipartFile file) throws IOException {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        assert md != null;
        md.update(file.getInputStream().readAllBytes());
        return md.digest();
    }

    private AudioEntity makeEntity(final String author, final String title, final String path, final String checkSum) {
        return new AudioEntity(author, title, path, checkSum, userLogic.getCurrentUser());
    }

}