package com.petproject.yoba_audio.entities;

import com.petproject.yoba_audio.dto.ConvertibleDto;

public interface ConvertibleEntity<T extends ConvertibleDto> {
    T convertToDto();
}
