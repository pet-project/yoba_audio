package com.petproject.yoba_audio.entities;


import com.petproject.yoba_audio.dto.AudioDto;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "AudioFiles")
public class AudioEntity implements ConvertibleEntity<AudioDto> {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator="UUID")
    @GenericGenerator(name="UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Column(name = "md5", nullable = false)
    private String checkSum;

    @Column(name = "author", nullable = false)
    private String author;
    @Column(name = "title", nullable = false)
    private String title;
    @ManyToOne
    @JoinColumn(name = "privateUserData_id")
    private PrivateUserData owner;

    @Column(name = "fileName", nullable = false)
    private String fileName;

    public AudioEntity() {
    }

    public AudioEntity(UUID id, String author, String title, String fileName, String checkSum, PrivateUserData owner) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.fileName = fileName;
        this.checkSum = checkSum;
        this.owner = owner;
    }

    public AudioEntity(String author, String title, String fileName, String checkSum, PrivateUserData owner) {
        this.checkSum = checkSum;
        this.author = author;
        this.title = title;
        this.owner = owner;
        this.fileName = fileName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public PrivateUserData getOwner() {
        return owner;
    }

    public void setOwner(PrivateUserData owner) {
        this.owner = owner;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    @Override
    public AudioDto convertToDto(){
        return new AudioDto(this.getId(),this.getCheckSum(),this.getAuthor(), this.getTitle(), this.getOwner().getId(), this.getFileName());
    }

    /*@Override
    public List<AudioDto> convertToDto(List<ConvertableEntity> entities) {
        return entities.stream().map((s)->s.convertToDto()).collect(Collectors.toList());

    }*/

}
