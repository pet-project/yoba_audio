package com.petproject.yoba_audio.dto;

import com.petproject.yoba_audio.entities.AudioEntity;

import java.util.UUID;

public class AudioDto implements ConvertibleDto<AudioEntity> {
    private UUID id;
    private String checkSum;
    private String author;
    private String title;
    private UUID owner;
    private String fileName;

    public AudioDto(UUID id, String checkSum, String author, String title, UUID owner, String fileName) {
        this.id = id;
        this.checkSum = checkSum;
        this.author = author;
        this.title = title;
        this.owner = owner;
        this.fileName = fileName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UUID getOwner() {
        return owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public AudioEntity convertToEntity() {
        return null;
    }
}
