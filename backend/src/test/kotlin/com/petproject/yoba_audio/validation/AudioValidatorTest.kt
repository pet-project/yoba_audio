package com.petproject.yoba_audio.validation

import com.petproject.yoba_audio.dto.FileUploadDto
import com.petproject.yoba_audio.enums.Error.NO_FILE_PROVIDED_MSG
import com.petproject.yoba_audio.enums.Error.ONLY_AUDIO_FILE_SUPPORTS_MSG
import org.junit.jupiter.api.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.springframework.validation.Errors
import org.springframework.web.multipart.MultipartFile

class AudioValidatorTest {

    private val `class` = MultipartFile::class.java
    private val file = mock(`class`)
    private val dto = FileUploadDto(file = file)

    @Captor
    private val errorCaptor: ArgumentCaptor<String> = ArgumentCaptor.forClass(String::class.java)
    private val errorMessages = mutableMapOf(
        NO_FILE_PROVIDED_MSG.message to 0,
        ONLY_AUDIO_FILE_SUPPORTS_MSG.message to 0
    )
    private val errors = mock(Errors::class.java).apply {
        `when`(reject(errorCaptor.capture())).then {
            errorMessages.merge(errorCaptor.value, 1, Int::plus)
        }
    }

    private val audioValidator = AudioValidator()

    @Test
    fun test() {
        assert(audioValidator.supports(dto.javaClass))
        file.apply {
            `when`(isEmpty).thenReturn(true)
            `when`(contentType).thenReturn("audio/t")
        }
        audioValidator.validate(dto, errors)

        assert(errorMessages[NO_FILE_PROVIDED_MSG.message] == 1)

        file.apply {
            `when`(isEmpty).thenReturn(false)
            `when`(contentType).thenReturn("")
        }
        audioValidator.validate(dto, errors)
        assert(errorMessages[ONLY_AUDIO_FILE_SUPPORTS_MSG.message] == 1)
    }
}