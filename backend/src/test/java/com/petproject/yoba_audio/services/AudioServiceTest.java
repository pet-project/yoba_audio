package com.petproject.yoba_audio.services;

import com.petproject.yoba_audio.entities.AudioEntity;
import com.petproject.yoba_audio.repositories.AudioFilesCRUDRepository;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AudioServiceTest {

    private static final String TEST_UPLOAD_FOLDER = "./testUploadFolder";

    private final AudioEntity audio = new AudioEntity();

    private final AudioFilesCRUDRepository audioFilesCRUDRepository = mock(AudioFilesCRUDRepository.class);
    private final AudioService audioService = new AudioService(audioFilesCRUDRepository, TEST_UPLOAD_FOLDER);

    @BeforeAll
    public static void setupBefore() throws IOException {
        final Path path = Paths.get(TEST_UPLOAD_FOLDER);
        if (Files.exists(path)) {
            Files.walk(path)
                 .map(Path::toFile)
                 .forEach(File::delete);
        }
    }

    @BeforeEach
    public void setup() {
        audio.setId(UUID.randomUUID());
        audio.setAuthor("someAuthor");
        audio.setTitle("someTitle");
        audio.setFileName("fileName.wav");
        audio.setCheckSum(computeCheckSum(audio));

        final Map<UUID, AudioEntity> storage = new HashMap<>();

        when(audioFilesCRUDRepository.save(any()))
                .thenAnswer((InvocationOnMock invocation) -> putIn(storage, invocation.getArgument(0)));

        when(audioFilesCRUDRepository.findById(any()))
                .thenAnswer((InvocationOnMock invocation) -> Optional.ofNullable(
                        storage.get(invocation.getArgument(0))
                ));
    }

    private String computeCheckSum(final AudioEntity audio) {
        return computeCheckSum(audio.getAuthor(), audio.getTitle());
    }

    private String computeCheckSum(final String author, final String title) {
        return author + title;
    }

    private AudioEntity putIn(final Map<UUID, AudioEntity> storage, final AudioEntity audio) {
        storage.put(audio.getId(), audio);
        return audio;
    }

    @Test
    void uploadAudioFile() throws IOException {
        MultipartFile file = new MockMultipartFile(audio.getFileName(), new byte[]{0});

        final Path path = Paths.get(TEST_UPLOAD_FOLDER + "/" + audio.getFileName());

        assertFalse(path.toFile().exists());
        assertNotNull(audioService.uploadAudioFile(file, path, audio));
        assertTrue(path.toFile().exists());

    }

    @Test
    void downloadAudioFile() throws Exception {
        audioFilesCRUDRepository.save(audio);

        Resource resource = audioService.getAudioFile(audio.getId());
        assertEquals(getPath(audio), resource.getURI().getPath());
    }

    private String getPath(final AudioEntity audio) {
        return Paths.get(audio.getFileName()).toUri().getPath();
    }
}