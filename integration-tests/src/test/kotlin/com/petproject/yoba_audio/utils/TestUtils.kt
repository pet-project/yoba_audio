package com.petproject.yoba_audio.utils

import com.petproject.yoba_audio.controllers.*
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.stereotype.Component
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@Component
class TestUtils(private val mockMvc: MockMvc) {

    fun register(userName: String, password: String): ResultActions = mockMvc.perform(
        post(PUBLIC_CONTROLLER_PATH + REGISTER)
            .content(registrationJson(userName, password))
            .contentType(APPLICATION_JSON)
    )

    fun login(userName: String, password: String): ResultActions = mockMvc.perform(
        post(PUBLIC_CONTROLLER_PATH + LOGIN)
            .content(loginJson(userName, password))
            .contentType(APPLICATION_JSON)
    )

    fun getCurrentUser(token: String): ResultActions = mockMvc.perform(
        get(SECURED_USER_CONTROLLER_PATH + CURRENT)
            .header("Authorization", "Bearer $token")
    )

    private fun registrationJson(userName: String, password: String) =
        """{"username": "$userName", "password": "$password"}"""

    private fun loginJson(userName: String, password: String)=
        """{"username": "$userName", "password": "$password"}"""

}