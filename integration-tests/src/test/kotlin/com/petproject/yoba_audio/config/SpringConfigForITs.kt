package com.petproject.yoba_audio.config

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.*
import org.springframework.web.client.RestTemplate

const val TEST_PROFILE = "test"

@Configuration
@Profile(TEST_PROFILE)
@EnableAspectJAutoProxy
@ComponentScan(basePackages = ["com.petproject.yoba_audio"])
@PropertySource(value = ["classpath:/application.properties"])
open class SpringConfigForITs {

    @Bean
    open fun restTemplate(): RestTemplate = RestTemplateBuilder().build()

}