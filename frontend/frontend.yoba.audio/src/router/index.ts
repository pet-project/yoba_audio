import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import TokenHelper from '@/helpers/TokenHelper';

const middleware = (next: Function) => (TokenHelper.getToken('accessToken') ? next() : next('/auth/login/'));

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Player',
    component: () => import(/* webpackChunkName: "player" */ '@/views/Player.vue'),
    beforeEnter: async (to, from, next) => middleware(next),
  },
  {
    path: '/auth/',
    name: 'auth',
    redirect: '/auth/login/',
    component: () => import(/* webpackChunkName: "auth" */ '@/views/Auth.vue'),
    children: [
      {
        path: 'login/',
        name: 'login',
        component: () => import(/* webpackChunkName: "auth" */ '@/views/Login.vue'),
      },
      {
        path: 'registration/',
        name: 'registration',
        component: () => import(/* webpackChunkName: "auth" */ '@/views/Registration.vue'),
      },
    ],
  },
  {
    path: '/user',
    name: 'User',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "user" */ '@/views/User.vue'),
    beforeEnter: async (to, from, next) => middleware(next),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
